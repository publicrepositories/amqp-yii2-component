Pluralsight/AMPQ
=============

AMQP Component for Yii2

Table of Contents
-----------------

1. [Introduction](#introduction)
2. [Configuration](#config)
3. [Yii2 component](#yii2)
4. [Examples](#examples)
   * [How to use Yii2 component]

Introduction
------------
The purpose of this component is to make it easy to implement a RabbitMQ AMQP service inside of Yii.  

Configuration
--------

Yii2 component
--------


Examples
--------

### How to use Yii2 component

Configuration Example:
'component' => [
    'amqp' => [
        'class' => 'luralsight\amqp\AMQPComponent',
        'options' => [
            'host' => '52.10.143.51',
            'user' => 'stagerabbit',
            'password' => 'GjBh48MDd431p42PWg4!',
            'port' => '5672',
            'vhost' => '/',
        ],
        'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
    ],
],



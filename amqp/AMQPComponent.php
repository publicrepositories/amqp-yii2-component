<?php
    namespace pluralsight\amqp;

    use Yii;
    use Yii\base\Component;
    use yii\base\InvalidConfigException;

    use PhpAmqpLib\Message\AMQPMessage;
    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use PhpAmqpLib\Exception\AMQPTimeoutException;

    class AMQPComponent extends Component
    {
        public $options = [];
        public $client;

        public function init()
        {
            parent::init();
            $this->checkConfiguration();
            $this->setClient();
            $this->createConnection();
        }

        public function setClient(\pluralsight\amqp\RabbitClient $client=null)
        {
            if ( isset($client) )
            {
                $this->client = $client;
            }
            else
            {
                $container = new yii\di\Container;
                $this->client = $container->get($this->client['class']);
            }

        }

        public function createConnection()
        {
            $this->client->createConnection($this->options['host'], $this->options['port'], $this->options['user'], $this->options['password'], $this->options['vhost']);
            $this->createChannel();
        }

        public function createChannel()
        {
            $this->client->createChannel();
        }

        public function setExchange($exchange, $type='fanout', $passive=false, $durable=true, $auto_delete=false)
        {
            $this->client->setExchange($exchange, $type, $passive, $durable, $auto_delete);

            return $this;
        }

        public function getExchange()
        {
            return $this->client->getExchange();
        }

        public function setQueue($queue, $passive=false, $durable=true, $exclusive=false, $auto_delete=false)
        {
            $this->client->setQueue($queue, $passive, $durable, $exclusive, $auto_delete);

            return $this;
        }

        public function getQueue()
        {
            return $this->client->getQueue();
        }

        public function bindQueueToExchange($queue, $exchange)
        {
            $this->client->bindQueueToExchange($queue, $exchange);

            return $this;
        }

        public function send($message, $properties=[])
        {
            $this->client->send($message, $properties);

            return $this;
        }

        public function getOneMessage($ticket=null)
        {
            return $this->client->getOneMessage($ticket);
        }

        public function pollMessages($consumerTag='', $noLocal=false, $exclusive=false, $noWait=false, $callback='', $ticket=null, $arguments=[])
        {
            $this->client->pollMessages($consumerTag, $noLocal, $exclusive, $noWait, $callback, $ticket, $arguments);

            return $this;
        }

        public function setAcknowledgement($acknowledgmentOn = false)
        {
            $this->client->setAcknowledgement($acknowledgmentOn);

            return $this;
        }

        public function markMessageProcessed($message)
        {
            $this->client->markMessageProcessed($message->delivery_info['delivery_tag']);

            return $this;
        }

        protected function messageConsumeError($error)
        {
            //$error = new pluralsight\logging\error;
           // Yii::error($error, __METHOD__);
        }

        public function messageCallback($message)
        {
            $this->client->setMessage($message);

            return $this;
        }

        public function checkConfiguration()
        {
            if (empty($this->options))
                throw new InvalidConfigException("Invalid amqp configuration: 'options'");

            if (!isset($this->options['host']) || !$this->options['host'])
                throw new InvalidConfigException("Invalid amqp configuration: 'options/host'");

            if (!isset($this->options['port']) || !$this->options['port'])
                throw new InvalidConfigException("Invalid amqp configuration: 'options/port'");

            if (!isset($this->options['vhost']) || !$this->options['vhost'])
                throw new InvalidConfigException("Invalid amqp configuration: 'options/vhost'");

            if (!isset($this->options['user']) || !$this->options['user'])
                throw new InvalidConfigException("Invalid amqp configuration: 'options/user'");

            if (!isset($this->options['password']) || !$this->options['password'])
                throw new InvalidConfigException("Invalid amqp configuration: 'options/password'");

            //if (!$this->client instanceof \pluralsight\amqp\AMQPClient)
            //    throw new InvalidConfigException("Invalid amqp configuration: 'client'");

            return true;
        }
    }

<?php

    namespace pluralsight\amqp;

    interface AMQPClient
    {
        public function createConnection($host, $port, $user, $password, $vhost);

        public function createChannel(\PhpAmqpLib\Channel\AMQPChannel $channel=null);

        public function setExchange($exchange, $type='fanout', $passive=false, $durable=true, $auto_delete=false);

        public function setQueue($queue, $passive=false, $durable=true, $exclusive=false, $auto_delete=false);

        public function bindQueueToExchange($queue, $exchange);

        public function send($message, $properties=[]);

        public function setAcknowledgement($acknowledgmentOn = false);

        public function markMessageProcessed($deliveryTag);

        public function pollMessages($consumerTag='', $noLocal=false, $exclusive=false, $noWait=false, $callback, $ticket=null, $arguments=[]);

        public function getOneMessage($ticket=null);

        public function messageConsumeError($error);
    }

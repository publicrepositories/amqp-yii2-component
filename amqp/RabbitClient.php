<?php

    namespace pluralsight\amqp;

    use Yii;

    use PhpAmqpLib\Message\AMQPMessage;
    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use PhpAmqpLib\Exception\AMQPTimeoutException;

    class RabbitClient implements \pluralsight\amqp\AMQPClient
    {
        protected $connection = null;
        protected $channel = null;
        protected $queue = null;
        protected $exchange = null;
        protected $acknowledgment = true;

        public function createConnection($host, $port, $user, $password, $vhost)
        {
            $this->connection = new AMQPStreamConnection($host, $port, $user, $password, $vhost);
        }

        public function createChannel(\PhpAmqpLib\Channel\AMQPChannel $channel=null)
        {
            if ( isset($channel) )
                $this->channel = $channel;
            else
                $this->channel = $this->connection->channel();
        }

        public function getChannel()
        {
            return $this->channel;
        }

        public function setExchange($exchange, $type='fanout', $passive=false, $durable=true, $auto_delete=false)
        {
            $this->exchange = $exchange;
            $this->channel->exchange_declare($exchange, $type, $passive, $durable, $auto_delete);

            return $this;
        }

        public function setQueue($queue, $passive=false, $durable=true, $exclusive=false, $auto_delete=false)
        {
            $this->queue = $queue;
            $this->channel->queue_declare($queue, $passive, $durable, $exclusive, $auto_delete);

            return $this;
        }

        public function getExchange()
        {
            return $this->exchange;
        }

        public function getQueue()
        {
            return $this->queue;
        }

        public function bindQueueToExchange($queue, $exchange)
        {
            $this->setQueue($queue);
            $this->channel->queue_bind($queue, $exchange);

            return $this;
        }

        public function send($message, $properties=[])
        {
            if (!is_string($message))
                $message = json_encode($message);

            if (empty($properties))
                $message = new AMQPMessage($message);
            else
                $message = new AMQPMessage($message, $properties);

            $this->channel->basic_publish($message, $this->exchange);

            return $this;
        }

        public function setAcknowledgement($acknowledgmentOn = false)
        {
            if ($acknowledgmentOn)
                $this->acknowledgment = false;
            else
                $this->acknowledgment = true;

            return $this;
        }

        public function getAcknowledgement()
        {
            return $this->acknowledgment;
        }

        public function markMessageProcessed($deliveryTag)
        {
            $this->channel->basic_ack($deliveryTag);

            return $this;
        }

        /** Do not use this function, it needs thought thru better first */
        public function pollMessages($consumerTag='', $noLocal=false, $exclusive=false, $noWait=false, $callback=null, $ticket=null, $arguments=[])
        {
            $this->channel->basic_consume(
                $this->queue,
                $consumerTag,
                $noLocal,
                $this->acknowledgment,
                $exclusive,
                $noWait,
                $callback,
                $ticket,
                $arguments
            );

            while (count($this->channel->callbacks))
            {
                try {
                    $this->channel->wait();
                } catch (AMQPTimeoutException $e) {
                    $this->messageConsumeError($e);
                    break;
                }
            }

            return $this;
        }

        public function getOneMessage($ticket=null)
        {
            return $this->channel->basic_get(
                $this->queue,
                $this->acknowledgment,
                $ticket
            );
        }

        public function messageConsumeError($error)
        {
            //$error = new pluralsight\logging\error;
            //Yii::error($error, __METHOD__);
        }
    }

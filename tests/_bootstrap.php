<?php
// This is global bootstrap for autoloading
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

// fcgi doesn't have STDIN and STDOUT defined by default
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'w'));

defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', dirname(dirname(dirname(dirname(__DIR__)))));

require_once(YII_APP_BASE_PATH . '/vendor/autoload.php');

$kernel = \AspectMock\Kernel::getInstance();
$kernel->init([
    'debug' => true,
    'appDir' => YII_APP_BASE_PATH,
    'includePaths' => [YII_APP_BASE_PATH . '/vendor/pluralsight/amqp/amqp', YII_APP_BASE_PATH . '/vendor/yiisoft'],
    'excludePaths' => [YII_APP_BASE_PATH . '/vendor/pluralsight/amqp/tests'],
]);
$kernel->loadFile(YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php');
Yii::setAlias('api', dirname(__DIR__));
Yii::setAlias('common', YII_APP_BASE_PATH . '/common');
Yii::setAlias('console', YII_APP_BASE_PATH . '/console');
Yii::setAlias('tests', YII_APP_BASE_PATH . '/tests');

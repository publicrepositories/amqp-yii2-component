<?php
namespace tests;

use Codeception\Util\Stub;
use AspectMock\Test as test;
use yii\base\InvalidConfigException;

class AMQPComponentTest extends \Codeception\TestCase\Test
{
    /**
     * @var \tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {   
    }

    protected function _after()
    {
    }

    public function testInit()
    {
        test::double('\pluralsight\amqp\AMQPComponent', [
            'checkConfiguration'=>true,
            'setClient'=>true,
            'createConnection'=>true
        ]);

        $amqpObj = new \pluralsight\amqp\AMQPComponent();

        $amqpProxy = test::double($amqpObj, [
            'checkConfiguration'=>true,
            'setClient'=>true,
            'createConnection'=>true
        ]);

        $amqpObj->init();

        $this->assertNull( $amqpProxy->verifyInvokedOnce('checkConfiguration') );
        $this->assertNull( $amqpProxy->verifyInvokedOnce('setClient') );
        $this->assertNull( $amqpProxy->verifyInvokedOnce('createConnection') );
    }

    public function testSetExchange()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $testExchangeVal = 'testQueue';

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['setExchange'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('setExchange')
            ->with($testExchangeVal, $type='fanout', $passive=false, $durable=true, $auto_delete=false)
            ->willReturn('set_exchange_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $this->assertNull($amqp->getExchange());
        $return = $amqp->setExchange($testExchangeVal);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testSetQueue()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $testQueueVal = 'testQueue';

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['setQueue'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('setQueue')
            ->with($testQueueVal, $passive=false, $durable=true, $exclusive=false, $auto_delete=false)
            ->willReturn('set_queue_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $this->assertNull($amqp->getQueue());
        $return = $amqp->setQueue($testQueueVal);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testBindQueueToExchange()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $testQueueVal = 'testQueue';
        $testExchangeVal = 'testQueue';

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['bindQueueToExchange', 'setQueue'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('bindQueueToExchange')
            ->with($testQueueVal, $testExchangeVal)
            ->willReturn('bind_queue_exchange_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $this->assertNull($amqp->getQueue());
        $this->assertNull($amqp->getExchange());
        $return = $amqp->bindQueueToExchange($testQueueVal, $testExchangeVal);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testSend()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $message = 'testMessage';

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['send'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('send')
            ->with($message, $properties=[])
            ->willReturn('send_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $return = $amqp->send($message);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testPollMessages()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['pollMessages'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('pollMessages')
            ->with($consumerTag='', $noLocal=false, $exclusive=false, $noWait=false, $callback='', $ticket=null, $arguments=[])
            ->willReturn('pollMessages_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $return = $amqp->pollMessages();

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testSetAcknowledgement()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $acknowledgmentOn = true;

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['setAcknowledgement'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('setAcknowledgement')
            ->with($acknowledgmentOn)
            ->willReturn('setAcknowledgement_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $return = $amqp->setAcknowledgement($acknowledgmentOn);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testMarkMessageProcessed()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $tag = 'testTag';
        $message = (object) ['delivery_info'=>['delivery_tag'=>$tag]];

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['markMessageProcessed'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('markMessageProcessed')
            ->with($tag)
            ->willReturn('message_processed_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $return = $amqp->markMessageProcessed($message);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testMessageCallback()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $message = 'testMessage';

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['setMessage'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('setMessage')
            ->with($message)
            ->willReturn('set_message_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $return = $amqp->messageCallback($message);

        $this->assertInstanceOf('pluralsight\amqp\AMQPComponent', $return);
    }

    public function testGetOneMessage()
    {
        test::double( '\pluralsight\amqp\AMQPComponent', [ 'init'=>true ] );

        $message = 'testMessage';

        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
            ->setMethods(['getOneMessage'])
            ->getMock();

        $mockRabbitClient->expects($this->once())
            ->method('getOneMessage')
            ->with($message)
            ->willReturn('getOneMessage_test');

        $amqp = $this->createTestAMQP($mockRabbitClient);

        $amqp->getOneMessage($message);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'options'
     */
    public function testMissingOptions()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
            'options' => []
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'options/host'
     */
    public function testMissingHost()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
            'options' => [
                    'host' => '',
                    'user' => 'foo',
                    'password' => 'bar',
                    'port' => '5672',
                    'vhost' => '/'
                ],
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'client'
     */
    public function testBadClientInterface()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'options' => [
                    'host' => '192.168.33.10',
                    'user' => 'foo',
                    'password' => 'bar',
                    'port' => '5672',
                    'vhost' => '/'
                ]
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'options/port'
     */
    public function testMissingPort()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
            'options' => [
                    'host' => '192.168.33.10',
                    'user' => 'foo',
                    'password' => 'bar',
                    'port' => '',
                    'vhost' => '/'
                ]
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'options/vhost'
     */
    public function testMissingVhost()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
            'options' => [
                    'host' => '192.168.33.10',
                    'user' => 'foo',
                    'password' => 'bar',
                    'port' => '5672',
                    'vhost' => ''
                ]
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'options/user'
     */
    public function testMissingUser()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
            'options' => [
                    'host' => '192.168.33.10',
                    'user' => '',
                    'password' => 'bar',
                    'port' => '5672',
                    'vhost' => '/'
                ]
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    /**
     * @expectedException \yii\base\InvalidConfigException
     * @expectedExceptionMessage Invalid amqp configuration: 'options/password'
     */
    public function testMissingPassword()
    {
        $badConfig = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => ['class' => 'pluralsight\amqp\RabbitClient',],
            'options' => [
                    'host' => '192.168.33.10',
                    'user' => 'foo',
                    'password' => '',
                    'port' => '5672',
                    'vhost' => '/'
                ]
            ];

        $amqp = \Yii::createObject($badConfig);
    }

    private function _mockRabbitClient()
    {
        $mockRabbitClient = $this->getMockBuilder('pluralsight\amqp\RabbitClient')
                                 ->getMock();

        $mockConnection = $this->getMockBuilder('PhpAmqpLib\Connection\AMQPStreamConnection')
                               ->disableOriginalConstructor()
                               ->getMock();

        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
                            ->disableOriginalConstructor()
                            ->getMock();

        $mockChannel->method('exchange_declare')
            ->willReturn('exchange_declare_test');

        $mockChannel->method('queue_declare')
            ->willReturn('queue_declare_test');

        $mockConnection->method('channel')
            ->willReturn($mockChannel);

        $mockRabbitClient->method('createConnection')
                         ->willReturn($mockConnection);

        return $mockRabbitClient;
    }

    private function createTestAMQP($client=null) {
        if (isset($client))
        {
            $mockRabbitClient = $client;
        }
        else
        {
            $mockRabbitClient = $this->_mockRabbitClient();
        }

        $config = [
            'class' => 'pluralsight\amqp\AMQPComponent',
            'client' => $mockRabbitClient,
            'options' => [
                'host' => '192.168.33.10',
                'user' => 'foo',
                'password' => 'bar',
                'port' => '5672',
                'vhost' => '/'
            ]
        ];

        $amqp = \Yii::createObject($config);

        return $amqp;
    }

}
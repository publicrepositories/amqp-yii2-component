<?php
namespace tests;

use pluralsight\amqp\RabbitClient;

class RabbitClientTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSetExchange()
    {
        $exchange = 'exchange';
        $exchangeParams = ['exchangeTest', 'fanout2', true, false, true];

        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();

        $mockChannel
            ->expects($this->exactly(2))
            ->method('exchange_declare')
            ->withConsecutive(
                [$this->equalTo($exchange), $this->equalTo('fanout'), $this->isFalse(), $this->isTrue(), $this->isFalse()],
                [$this->equalTo($exchangeParams[0]), $this->equalTo($exchangeParams[1]), $this->isTrue(), $this->isFalse(), $this->isTrue()]
            )
            ->willReturn('$mockChannel');

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);
        $this->assertInstanceOf('PhpAmqpLib\Channel\AMQPChannel', $mockRabbitClient->getChannel());
        $this->assertEquals($mockChannel, $mockRabbitClient->getChannel());
        $return = $mockRabbitClient->setExchange($exchange);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
        $this->assertEquals($exchange, $mockRabbitClient->getExchange());
        $return = $mockRabbitClient->setExchange($exchangeParams[0], $exchangeParams[1], $exchangeParams[2], $exchangeParams[3], $exchangeParams[4]);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
        $this->assertEquals($exchangeParams[0], $mockRabbitClient->getExchange());
    }

    public function testSetQueue()
    {
        $queueParams = ['queueTest2', true, false, true, true];
        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();

        $mockChannel
            ->expects($this->once())
            ->method('queue_declare')
            ->with($this->equalTo($queueParams[0]), $this->isTrue(), $this->isFalse(), $this->isTrue(), $this->isTrue())
            ->willReturn('$mockChannel');

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);

        $return = $mockRabbitClient->setQueue($queueParams[0], $queueParams[1], $queueParams[2], $queueParams[3], $queueParams[4]);

        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
        $this->assertEquals($queueParams[0], $mockRabbitClient->getQueue());
    }

    public function testBindQueueToExchange()
    {
        $testParams = ['queue', 'exchange'];
        $testParams2 = ['queue2', 'exchange2'];

        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();

        $mockChannel
            ->expects($this->exactly(2))
            ->method('queue_bind')
            ->withConsecutive(
                [$this->equalTo($testParams[0]), $this->equalTo($testParams[1])],
                [$this->equalTo($testParams2[0]), $this->equalTo($testParams2[1])]
            )
            ->willReturn('$mockChannel');

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);
        $return = $mockRabbitClient->bindQueueToExchange($testParams[0], $testParams[1]);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
        $this->assertEquals($testParams[0], $mockRabbitClient->getQueue());
        $return = $mockRabbitClient->bindQueueToExchange($testParams2[0], $testParams2[1]);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
        $this->assertEquals($testParams2[0], $mockRabbitClient->getQueue());
    }

    public function testPollMessages()
    {
        $testQueue = 'testQueue';
        $testTag = $testCallback = $testTicket = 'testValue';
        $testArgs = ['testArg1', 'testArg2'];
        $testLocal = $testExclusive = $testWait = $testAcknowledgment = true;
        $testParams = [$testTag, $testLocal, $testExclusive, $testWait, $testCallback, $testTicket, $testArgs];
        $testParams2 = ['', !$testLocal, !$testExclusive, !$testWait, null, null, []];

        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();

        $mockChannel
            ->expects($this->exactly(2))
            ->method('basic_consume')
            ->withConsecutive(
                [ $this->equalTo($testQueue), $this->equalTo($testParams[0]), $this->equalTo($testParams[1])
                    , $this->equalTo($testAcknowledgment), $this->equalTo($testParams[2]), $this->equalTo($testParams[3])
                    , $this->equalTo($testParams[4]), $this->equalTo($testParams[5]), $this->equalTo($testParams[6]) ],
                [ $this->equalTo($testQueue), $this->equalTo($testParams2[0]), $this->equalTo($testParams2[1])
                    , $this->equalTo($testAcknowledgment), $this->equalTo($testParams2[2]), $this->equalTo($testParams2[3])
                    , $this->equalTo($testParams2[4]), $this->equalTo($testParams2[5]), $this->equalTo($testParams2[6]) ]
            )
            ->willReturn('$mockChannel');

//        $mockChannel
//            ->expects($this->exactly(2))
//            ->method('wait');

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);
        $mockRabbitClient->setQueue($testQueue);
        $mockRabbitClient->setAcknowledgement(!$testAcknowledgment);

        $return = $mockRabbitClient->pollMessages($testParams[0], $testParams[1], $testParams[2], $testParams[3]
            , $testParams[4], $testParams[5], $testParams[6]);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);

        $return = $mockRabbitClient->pollMessages();
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
    }

    public function testSend()
    {
        $message = 'testMessage';
        $exchange = 'exchange';
        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();
        $mockChannel
            ->expects($this->once())
            ->method('basic_publish')
            ->with($this->isInstanceOf('PhpAmqpLib\Message\AMQPMessage'), $this->equalTo($exchange))
            ->willReturn('$mockChannel');

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);
        $mockRabbitClient->setExchange($exchange);
        $return = $mockRabbitClient->send($message);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
    }

    public function testMarkMessageProcessed()
    {
        $testTag = 'testTag';
        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();
        $mockChannel
            ->expects($this->once())
            ->method('basic_ack')
            ->with($this->equalTo($testTag))
            ->willReturn('$mockChannel');

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);
        $return = $mockRabbitClient->markMessageProcessed($testTag);
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
    }

    public function testGetOneMessage()
    {
        $testQueue = $testTicket = 'testValue';
        $testMessage = 'testMessage';
        $testAcknowlegement = true;

        $mockChannel = $this->getMockBuilder('PhpAmqpLib\Channel\AMQPChannel')
            ->disableOriginalConstructor()
            ->getMock();

        $mockChannel
            ->expects($this->once())
            ->method('basic_get')
            ->with($this->equalTo($testQueue), $this->equalTo($testAcknowlegement), $this->equalTo($testTicket))
            ->willReturn($testMessage);

        $mockRabbitClient = new RabbitClient();
        $mockRabbitClient->createChannel($mockChannel);
        $mockRabbitClient->setQueue($testQueue);
        $mockRabbitClient->setAcknowledgement();

        $message = $mockRabbitClient->getOneMessage($testTicket);
        $this->assertEquals($testMessage, $message);
    }

    public function testSetAcknowledgement()
    {
        $mockRabbitClient = new RabbitClient();

        $return = $mockRabbitClient->setAcknowledgement();
        $this->assertTrue($mockRabbitClient->getAcknowledgement());
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);

        $return = $mockRabbitClient->setAcknowledgement(true);
        $this->assertFalse($mockRabbitClient->getAcknowledgement());
        $this->assertInstanceOf('pluralsight\amqp\RabbitClient', $return);
    }
}
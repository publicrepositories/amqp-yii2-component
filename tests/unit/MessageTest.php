<?php
namespace tests;

date_default_timezone_set('UTC');

use pluralsight\amqp\messages\Message;
use Carbon\Carbon;
use Exception;

class FooMessage extends Message
{
    protected $messageName = 'foo';
}

class MessageTest extends \Codeception\TestCase\Test
{
    /**
     * @var \tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $knownDate = Carbon::create(2001, 5, 21, 12);          // create testing date
        Carbon::setTestNow($knownDate); 
    }

    public function testMessageName()
    {
        $msg = new FooMessage;

        $this->assertEquals('foo', $msg['messageName']);
    }

    public function testMessageTimestamp()
    {
        $msg = new FooMessage;

        $timestamp = $msg->messageInfo['timestamp'];

        $this->assertEquals('2001-05-21T12:00:00+0000', $timestamp);
    }
}